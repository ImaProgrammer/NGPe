// Our use cases
use std::thread;

const CORECLOCKSPEED: u32 = 331356228;

pub struct Cpu {
    pub regs: Registers,
    jit_mode: JitMode,
    instr_mode: InstructionSet,
}

impl Cpu {
    pub fn new() -> Cpu {
        let regs: Registers = Default::default();;

        Cpu {
            regs: regs,
            jit_mode: JitMode::Disabled,
            instr_mode: InstructionSet::ARM,
        }
    }

    pub fn set_jit_mode(&mut self, mode: JitMode) {
        self.jit_mode = mode;
    }
}

#[derive(Default)]
pub struct Registers {
    pub gpr: [u32; 13], // General purpose registers
    pub sp: u32,        // Stack pointer
    pub lr: u32,        // Link register
    pub pc: u32,        // Program counter
}

enum InstructionSet {
    ARM,
    Thumb,
    ThumbEE,
    Jazelle,
}

enum JitMode {
    Enabled,
    Disabled,
}
