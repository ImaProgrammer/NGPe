pub struct Sce {
    pub magic: u32,                 // 53434500 = SCE\0
    pub version: u32,               // Header Version 3
    pub sdk_type: u16,
    pub header_type: u16,           // 1 self, 2 unknown, 3 pkg
    pub metadata_offset: u32,       // Metadata offset
    pub header_len: u64,            // Self header length
    pub elf_filesize: u64,          // ELF file length
    pub sce_filesize: u64,          // SCE file length
    pub unknown: u64,               // Unknown
    pub sce_offset: u64,            // SCE offset
    pub app_info_offset: u64,       // App info offset
    pub elf_offset: u64,            // ELF #1 offset
    pub phdr_offset: u64,           // Program header offset
    pub shdr_offset: u64,           // Section header offset
    pub section_info_offset: u64,   // Section info offset
    pub sce_version_offset: u64,    // Version offset
    pub control_info_offset: u64,   // Control info offset
    pub control_info_size: u64,     // Control info size
    pub padding: u64,
}

pub struct AppInfo {
    pub auth_id: u64,   // Auth id
    pub vendor_id: u32, // Vendor id
    pub sce_type: u32,  // App type
    pub version: u64,   // App version
    pub padding: u64,   // Unknown
}

pub struct Elf {
    pub indent: [u8; 16],       // ELF Identification
    pub elf_type: u16,          // Object file type
    pub machine: u16,           // Machine type
    pub version: u32,           // Object file version
    pub entry: u64,             // Entry point address
    pub phoff: u64,             // Program header offset
    pub shoff: u64,             // Section header offset
    pub flags: u16,             // Processor specific flags
    pub eh_size: u32,           // ELF header size
    pub ph_entry_size: u16,     // Size of program header entry
    pub ph_num: u16,            // Number of program header entries
    pub entry_size: u16,     // Size of section header entry
    pub num: u16,            // Number of section header entries
    pub string_index: u16,   // Section name string table index
}

pub struct ElfProgram {
    pub elf_ph_type: u32,   // Type of segment
    pub flags: u32,         // Segment attributes
    pub offset: u64,        // Offset in file
    pub virt_addr: u64,     // Virtual address in memory
    pub phys_addr: u64,     // Reserved
    pub file_size: u64,     // Size of segment in file
    pub memory_size: u64,   // Size of segment in memory
    pub align: u64,         // Alignment of segment
}

pub struct ElfSection {
    pub name: u32,      // Section name
    pub sh_type: u32,   // Section type
    pub flags: u64,     // Section attributes
    pub addr: u64,      // Virtual address in memory
    pub offset: u64,    // Offset in file
    pub size: u64,      // Size of section
    pub link: u32,      // Link to other section
    pub info: u32,      // Miscellaneous information
    pub addralign: u64, // Address alignment boundary
    pub entsize: u64,   // Size of entries if section has table
}

pub struct SectionInfo {
    pub offset: u64,
    pub size: u64,
    pub compressed: u32,    // 2 = compressed
    pub unknown1: u32,
    pub encrypted: u32,     // 1 = encrypted
    pub unknown2: u32,
}

pub struct SceVersionInfo {
    pub unknown1: u32,
    pub unknown2: u32,
    pub unknown3: u32,
    pub unknown4: u32,
}

pub struct ControlInfo {
    pub file_digest50: FileDigest50,    // Type 4 0x50 bytes
    pub file_digest110: FileDigest110,  // Type 6 0x110 bytes
    pub file_digest50_2: FileDigest502, // Type 7 0x50 bytes
}

pub struct FileDigest50 {
    pub digest1: [u8; 20],  // Hash digest, same for every file
    pub digest2: [u8; 20],  // SHA-1 hash digest calculated of .elf file
    pub digest3: [u8; 12], 
    pub padding: u64,
}

pub struct FileDigest502 {
    pub unknown1: u64,
    pub unknown2: u64,
    pub unknown3: u64,
    pub unknown4: u64,
    pub unknown5: u64,
    pub unknown6: u64,
    pub unknown7: u64,
    pub unknown8: u64,
}

pub struct FileDigest110 {
     pub unknown1: u32,         // 0x00000001
     pub unknown2: [u8; 252],   
}
