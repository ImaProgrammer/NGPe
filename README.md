# NGPe
Researching the possibility of PS Vita emulation. Licensed under the terms of the GNU General Public License, version 3 or later (GPLv3).
You can chat on the official IRC channel on lolwutnet.
Network: apex.lolwutnet.xyz
Channel: #ngpe

## Requirments
- Latest version of Rust Stable

## Building
Use cargo run (path to ISO, arguments) with --release for a optimized build.